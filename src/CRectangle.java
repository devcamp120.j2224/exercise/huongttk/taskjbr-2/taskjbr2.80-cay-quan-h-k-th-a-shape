public class CRectangle extends CShape {

    public CRectangle(String color, boolean filled) {
        super(color, filled);
        //TODO Auto-generated constructor stub
    }
    private double width = 1.0;
    private double length = 1.0;
    public CRectangle(String color, boolean filled, double width, double length) {
        super(color, filled);
        this.width = width;
        this.length = length;
    }
    public CRectangle() {
        
    }
    public double getWidth() {
        return width;
    }
    public void setWidth(double width) {
        this.width = width;
    }
    public double getLength() {
        return length;
    }
    public void setLength(double length) {
        this.length = length;
    }
    /////////////////////////////
    public double getArea(){
        double area = this.length * this.width;
        return area;
    }
    public double getPerimeter(){
        double perimeter = (this.length + this.width)*2;
        return perimeter;
    }
    ////////////////////////////
    @Override
    public String toString() {
        return "CRectangle [length=" + length
        + ", width=" + width 
        + ", area =" + this.getArea()
        + ", perimeter =" + this.getPerimeter()
        
        + "]";
    }
}
