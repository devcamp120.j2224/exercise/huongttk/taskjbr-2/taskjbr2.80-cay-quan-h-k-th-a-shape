public class App {
    public static void main(String[] args) throws Exception {
        CShape Shape1 = new CShape();
        CShape Shape2 = new CShape("green", false);
        CShape Shape3 = new CShape("red", false);

        System.out.println(Shape1);
        System.out.println(Shape2);
        ////////////////////////////////
        CCircle vongtron1 = new CCircle();
        CCircle vongtron2 = new CCircle(Shape2.getColor(), Shape2.isFilled(), 1);
        System.out.println(vongtron1);
        System.out.println(vongtron2);
        ////////////////////////// 
        CRectangle hinhchunhat1 = new CRectangle();
        CRectangle hinhchunhat2 = new CRectangle(Shape3.getColor(), Shape3.isFilled(), 4, 5);
        System.out.println(hinhchunhat1);
        System.out.println(hinhchunhat2);
        ///////////////////
        CSquare hinhvuong1 = new CSquare();
        CSquare hinhvuong2 = new CSquare(Shape3.getColor(), Shape3.isFilled(), 5);
        System.out.println(hinhvuong1);
        System.out.println(hinhvuong2);
        
        
    }
}
