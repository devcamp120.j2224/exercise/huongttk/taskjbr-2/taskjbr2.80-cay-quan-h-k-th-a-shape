public class CCircle extends CShape{
    public CCircle(String color, boolean filled) {
        super(color, filled);
        //TODO Auto-generated constructor stub
    }

    private double radius = 0;

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public CCircle(String color, boolean filled, double radius) {
        super(color, filled);
        this.radius = radius;
    }
    public CCircle() {
        
    }
    ///////////////////////////////////////////////
    public double getArea(){
        double area = this.radius*this.radius*Math.PI ;
        return area;
    }
    public double getPerimeter(){
        double perimeter = this.radius*2*Math.PI;
        return perimeter;
    }

    ///////////////////////////////////////////////

    @Override
    public String toString() {
        return "CCircle [radius=" + radius
        + ", color=" + this.getColor() 
        + ", area=" + this.getArea() 
        + ", perimeter=" + this.getPerimeter() 
        + "]";
    }

    
}
