public class CShape {
    private String color = "";
    protected boolean filled = true;
    public CShape(String color, boolean filled) {
        this.color = color;
        this.filled = filled;
    }
    public CShape() {
        
        
    }
    public String getColor() {
        return color;
    }
    public void setColor(String color) {
        this.color = color;
    }
    public boolean isFilled() {
        return filled;
    }
    public void setFilled(boolean filled) {
        this.filled = filled;
    }
    @Override
    public String toString() {
        return "CShape [color=" + color + ", filled=" + filled + "]";
    }

}
