public class CSquare extends CRectangle {

    public CSquare(String color, boolean filled, double width) {
        super(color, filled);
        this.setWidth(width);
        this.setLength(width);
        //TODO Auto-generated constructor stub
    }

    public CSquare() {
    }

    @Override
    public String toString() {
        return "CSquare [length=" + this.getWidth()
        
        + ", area =" + this.getArea()
        + ", perimeter =" + this.getPerimeter()
        
        + "]";
    }

    

   
    
    

    
}